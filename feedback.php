<?php 
function sendRequest($url, $data = array(), $post = true)
    {
        $myCurl = curl_init();
        curl_setopt_array($myCurl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => $post,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_COOKIESESSION => true,
            CURLOPT_COOKIEJAR => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_COOKIEFILE => dirname(__FILE__) . '/cookie.txt'
        ));

        $response = curl_exec($myCurl);
        curl_close($myCurl);

        $result = json_decode($response, true);
        //echo "<pre>";
        //print_r($result[status]);
        //echo "</pre>";
        return $result;

    }
	
	
	$params = array(
	'action'        => 'status',
	'version'       => '3',
	'order_id'      => $_GET['id'] // унікальний id переданий з форми
	);
	$url = "https://www.liqpay.com/api/request";
	$public_key="i1496651840";
	$private_key="28JXfaO4cVgF1T9QkNjV5nbPrvp7ECd3mr6Z16Xx";
	 $data        = base64_encode(json_encode(array_merge(compact('public_key'), $params)));
        $signature   = base64_encode(sha1($private_key.$data.$private_key, 1));
        $postfields  = array(
           'data'  => $data,
           'signature' => $signature
        );